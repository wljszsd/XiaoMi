
function setCookie(name,value,day,path,domain,secure) {
    var strCookie="";

    if(name&&value){
        strCookie+=encodeURIComponent(name)+"="+encodeURIComponent(value)+";";
    }

    if(typeof(day)=="number"){
        var date=new Date();
        date.setDate(date.getDate()+day);
        strCookie+="expires="+date+";";

    }

    if(path){
        strCookie+="path"+path+";";
    }

    if(domain){
        strCookie+="domain"+domain+";";
    }

    if(secure){
        strCookie+="secure";

    }
    return document.cookie=strCookie;
}

function getCookieAll(){

    var obj={};
    var cookieStr=decodeURIComponent(document.cookie);//所有的cookie
    var arrList=cookieStr.split(";");

    for(var i=0;i<arrList.length;i++){
        var tmpArr=arrList[i].split("=")
        obj[tmpArr[0].trim()]=tmpArr[1]

    }
    return obj;
}

function getCookieByName(name) {
    var obj=getCookieAll();
    return obj[name];
}

function removeCookie(name) {
    var date=new Date();
    date.setDate(date.getDate()-1);
    document.cookie=encodeURIComponent(name)+"=;expires="+date.toUTCString();
}











