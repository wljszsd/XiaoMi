function getStyle(ele, attr) {
    if (ele.currentStyle) {
        return ele.currentStyle[attr]
    }
    return window.getComputedStyle(ele, null)[attr];
}

function animate(ele, target) {
    clearInterval(ele.timer);

    ele.timer = setInterval(() => {
        "use strict";
        let current = parseInt(getStyle(ele, "left"));
        let step = target > current ? +10 : -10;

        ele.style.left = ele.offsetLeft + step + "px";

        if (Math.abs(target - ele.offsetLeft) < Math.abs(step)) {
            clearInterval(ele.timer);
            ele.style.left = target + "px";
        }


    })

}